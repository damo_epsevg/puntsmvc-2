package edu.upc.damo.punts_mvc;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import java.util.Random;

public class MainActivity extends Activity {

    static final String TAG = "PROVA";
    static final int DIAMETRE = 25;

    private EditText t1;
    private EditText t2;

    private CjtDePunts cjtDePunts = new CjtDePunts();       // El Model
    private VistaDePunts vista;                          // La vista

    private Random generador = new Random();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();

        vista.defineixModel(cjtDePunts);
    }

    private void mostraCoordenades(Punt p) {
        t1.setText(String.valueOf(p.getX()));
        t2.setText(String.valueOf(p.getY()));
    }


    /* Inicialització de l'activity */

    private void inicialitza() {
        t1 = (EditText) findViewById(R.id.text1);
        t2 = (EditText) findViewById(R.id.text2);
        vista = (VistaDePunts) findViewById(R.id.vistaDePunts);

        final GeneradorPunt g = new GeneradorPunt();

        findViewById(R.id.botoVerd).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nouPunt(g, R.color.colorBotoVerd);
                    }
                }
        );

        findViewById(R.id.botoVermell).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nouPunt(g, R.color.colorBotoVermell);
                    }
                }
        );

        vista.setOnTouchListener(new View.OnTouchListener() {
                                     @Override
                                     public boolean onTouch(View v, MotionEvent event) {
                                         if (MotionEvent.ACTION_DOWN != event.getAction())
                                             return false;

                                         nouPunt(event, g, R.color.colorTouch);
                                         return true;
                                     }
                                 }
        );
    }

    private void nouPunt(MotionEvent event, GeneradorPunt g, int color) {
        Punt p = g.nouPunt(event, getResources().getColor(color));
        mostraCoordenades(p);
    }

    private void nouPunt(GeneradorPunt g, int color) {
        Punt p = g.nouPunt(getResources().getColor(color));
        mostraCoordenades(p);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void testIteracio() {
        CjtDePunts c = new CjtDePunts();

        c.afegeixPunt(new Punt(10, 10, Color.BLUE, 2));
        c.afegeixPunt(new Punt(1, 1, Color.BLUE, 2));
        c.afegeixPunt(new Punt(13, 31, Color.BLUE, 1));


        for (Punt p : c) {
            Log.i(TAG, p.toString());
        }
    }



      /* ---------------------    Class privade per a la generració dels punts -------------------*/

    private class GeneradorPunt {

        /**
         * @param event Conté la posició o s'ha de generar el punt
         * @param c     Color del punt
         */
        private Punt obtePunt(MotionEvent event, int c) {
            return new Punt(event.getX(), event.getY(), c, DIAMETRE);
        }

        /**
         * @param event Conté la posició o s'ha de generar el punt
         * @param c     Color del punt
         */
        private Punt nouPunt(MotionEvent event, int c) {
            Punt punt = obtePunt(event, c);
            cjtDePunts.afegeixPunt(punt);
            return punt;
        }

        private Punt nouPunt(int c) {
            Punt punt = obtePunt(c);
            cjtDePunts.afegeixPunt(punt);
            return punt;
        }

        private Punt obtePunt(int c) {
            /**
             * Genera un punt aleatori. El random retorna un valor entre 0 i 1, que considerem com una
             * fracció de l'espai disponible. Per això multipliquem el valor aleatori per la dimensió.
             * Per tal d'assegutar que el punt generat cap completament dins dels límits, hi sumem el diàmetre
             */

            return new Punt(generador.nextFloat() * vista.getWidth() + DIAMETRE,
                    generador.nextFloat() * vista.getHeight() + DIAMETRE,
                    c, DIAMETRE);
        }
    }


     /* --------------------------------------------------------------------------------------*/


}
